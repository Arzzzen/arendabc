var Storage = require('lib/Storage/Storage');

describe('Storage test', function() {
  describe('Server-side test', function() {
    beforeEach(function() {
      this.req = {
        cookies: {
          test: 'Foo'
        }
      };
      this.res = {};
      this.storage = new Storage(this.req, this.res);
    });
    it('should be an object', function() {
      expect(this.storage).toEqual(jasmine.objectContaining({
        get: jasmine.any(Function),
        set: jasmine.any(Function),
        remove: jasmine.any(Function)
      }));
    });
    it('should get right cookie', function() {
      expect(this.storage.get('test')).toBe('Foo');
    });
  });
  describe('Client-side test', function() {
    beforeEach(function() {
      process.browser = true;
      this.storage = new Storage;
    });
    afterEach(function() {
      process.browser = false;
    });
    it('should be an object', function() {
      expect(this.storage).toEqual(jasmine.objectContaining({
        get: jasmine.any(Function),
        set: jasmine.any(Function),
        remove: jasmine.any(Function)
      }));
    });
  });
});
