var app = require('app');
var Browser = require('zombie');
var port = 8080;

Browser.localhost('localhost', port);
Browser.silent = true;
app.set('port', port);

describe('Call request', function() {
  var server;
  var browser = new Browser();

  beforeAll(function(done) {
    server = app.listen(port, function() {
      browser.visit('/', function() {
        browser.clickLink('Заказать звонок', done);
      });
    });
  });

  afterAll(function(done) {
    server.close(done);
  });

  it('should be on page', function() {
    expect(browser.query('#callRequest')).not.toBe(null);
  });
  it('should show dialog on click', function() {
    expect(browser.query('.modal-dialog')).not.toBe(null);
    expect(browser.text('.modal-title')).toBe('Заказать звонок');
  });
  describe('submits form', function() {
    beforeAll(function() {
      browser
        .fill('phone', '+79111111111')
        .fill('name', 'Mr Someone');
      return browser.pressButton('Заказать!');
    });

    it('should be successful', function() {
      browser.assert.success();
    });

    it('should see success message', function() {
      expect(browser.text('.modal-body')).toBe('Спасибо, мы скорро свяжемся с вами!');
    });
  });
});
