var app = require('app');
var Browser = require('zombie');
var port = 8080;

Browser.localhost('localhost', port);
Browser.silent = true;
app.set('port', port);

describe('User visits home page', function() {
  var server;
  var browser = new Browser();

  beforeAll(function(done) {
    server = app.listen(port, function() {
      browser.visit('/', done);
    });
  });

  afterAll(function(done) {
    server.close(done);
  });

  it('should be successful', function() {
    expect(browser.success).toBe(true);
  });
  it('should see logo', function() {
    expect(browser.query('.page-logo a')).not.toBe(null);
  });
  it('should see city picker', function() {
    expect(browser.query('#city-picker button')).not.toBe(null);
    expect(browser.text('#city-picker button')).toBe('Санкт-Петербург');
  });
});
