var config = require('lib/Config');
var Router = function() {};

Router.prototype.getApiUrl = function(relative_url) {
  return config.api_server+'/'+relative_url+'.json';
}

module.exports = new Router();