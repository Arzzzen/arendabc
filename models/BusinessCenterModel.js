var Backbone = require('backbone');
var Router = require('router');
var parameterize = require('parameterize');

module.exports = Backbone.Model.extend({
  url: function() {
    return Router.getApiUrl('api/business_center/'+this.get('id'));
  },
  getUrl: function() {
    var slug = this.get('slug') ? this.get('slug') : parameterize(this.get('title'));
    return '/bc/'+this.get('id')+'-'+slug;
  }
}, {
  getIdFromUrl: function(id_or_url) {
    return isNaN(id_or_url) ? id_or_url.split('-')[0] : id_or_url;
  }
});