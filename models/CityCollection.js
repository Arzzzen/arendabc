var BaseCollection = require('lib/BaseCollection');
var CityModel = require('models/CityModel');
var Router = require('router');

module.exports = BaseCollection.extend({
  model: CityModel,
  url: Router.getApiUrl('api/cities')
});