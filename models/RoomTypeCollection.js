var BaseCollection = require('lib/BaseCollection');
var RoomTypeModel = require('models/RoomTypeModel');
var Router = require('router');

module.exports = BaseCollection.extend({
  model: RoomTypeModel,
  url: Router.getApiUrl('api/room_types')
});