var BaseCollection = require('lib/BaseCollection');
var MetroModel = require('models/MetroModel');
var Router = require('router');

module.exports = BaseCollection.extend({
  model: MetroModel,
  url: Router.getApiUrl('api/metros')
});