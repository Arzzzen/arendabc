var BaseCollection = require('lib/BaseCollection');
var Router = require('router');
var BusinessCenterModel = require('models/BusinessCenterModel');

module.exports = BaseCollection.extend({
  model: BusinessCenterModel,
  url: Router.getApiUrl('api/business_centers')
});