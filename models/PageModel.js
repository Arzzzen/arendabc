var Backbone = require('backbone');
var Router = require('router');
var parameterize = require('parameterize');

module.exports = Backbone.Model.extend({
  url: function() {
    return Router.getApiUrl('api/page/'+this.get('id'));
  }
}, {
  getIdFromUrl: function(slug) {
    return slug.split('-')[0];
  }
});