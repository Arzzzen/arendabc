var Backbone = require('backbone')
  , qs = require('query-string-object')
  , _ = require('underscore')
    ;

var SearchModel = Backbone.Model.extend({
    getUrl: function() {
      var currentUrlObject = SearchModel.getQueryParameters()
        , urlObject = _.extend(currentUrlObject, {s: this.toJSON() })
        , url = qs.stringify(urlObject)
        ;
      return '?'+url;
    },
    getData: function() {
      return {s: this.toJSON()};
    }
  },
  {
    getQueryParameters: function(str) {
      return qs(str || document.location.search.replace(/(^\?)/,''));
    }
  }
);

module.exports = SearchModel;