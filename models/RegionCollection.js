var BaseCollection = require('lib/BaseCollection');
var RegionModel = require('models/RegionModel');
var Router = require('router');

module.exports = BaseCollection.extend({
  model: RegionModel,
  url: Router.getApiUrl('api/regions')
});