var BaseCollection = require('lib/BaseCollection');
var ServiceModel = require('models/ServiceModel');
var Router = require('router');

module.exports = BaseCollection.extend({
  model: ServiceModel,
  url: Router.getApiUrl('api/services')
});