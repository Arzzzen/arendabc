var express = require('express')
  , React = require('react')
  , ReactDOM = require('react-dom/server')
  , router = express.Router()
  , BusinessCenterCollection = require('models/BusinessCenterCollection')
  , BusinessCenterModel = require('models/BusinessCenterModel')
  , CityCollection = require('models/CityCollection')
  , RegionCollection = require('models/RegionCollection')
  , MetroCollection = require('models/MetroCollection')
  , RoomTypeCollection = require('models/RoomTypeCollection')
  , ServiceCollection = require('models/ServiceCollection')
  , promise = require('node-promise')
  , Storage = require('lib/Storage/Storage')
  , config = require('lib/Config')
  , _ = require('underscore')
  ;

function defaultComponents(req, res) {
  var defer = promise.defer
    , deferred = new defer()
    , CityPicker = React.createFactory(require('components/CityPicker'))
    , cityCollection = new CityCollection()
    , storage = new Storage(req, res)
    , searchData = req.query.s || {}
    , SearchModel = require('models/SearchModel')
    , searchModel = new SearchModel(searchData)
    , CallRequest = React.createFactory(require('components/CallRequest'))
    , renderedComponents = {}
    , FavoritesMini = React.createFactory(require('components/FavoritesMini'))
    ;
    renderedComponents['preloader'] = {};
    renderedComponents['call_request'] = ReactDOM.renderToString(CallRequest());
    renderedComponents['favorites_mini'] = ReactDOM.renderToString(FavoritesMini({storage: storage}));
    cityCollection.fetch()
      .done(function() {
        renderedComponents['citi_picker'] = ReactDOM.renderToString(CityPicker({
          cities: cityCollection,
          searchModel: searchModel
        }));
        renderedComponents.preloader['cityCollection'] = cityCollection.toJSON();
        deferred.resolve(renderedComponents);
      });
  return deferred.promise;
}

/* GET home page. */
router.get('/', function(req, res, next) {
  var ComplexBCSearch = React.createFactory(require('components/ComplexBCSearch/ComplexBCSearch'))
    , bcCollection = new BusinessCenterCollection()
    , regionCollection = new RegionCollection()
    , metroCollection = new MetroCollection()
    , roomTypeCollection = new RoomTypeCollection()
    , serviceCollection = new ServiceCollection()
    , storage = new Storage(req, res)
    , searchData = req.query.s || {}
    , SearchModel = require('models/SearchModel')
    , searchModel = new SearchModel(searchData)
    , mapCollection = new BusinessCenterCollection()
    ;
  promise.all(
    defaultComponents(req, res),
    bcCollection.fetch({data: searchModel.getData()}),
    regionCollection.fetch(),
    metroCollection.fetch(),
    roomTypeCollection.fetch(),
    serviceCollection.fetch()
    )
    .then(function(result) {
      var complSearch = ReactDOM.renderToString(ComplexBCSearch({
          collection: bcCollection,
          storage: storage,
          searchModel: searchModel,
          regionCollection: regionCollection,
          metroCollection: metroCollection,
          roomTypeCollection: roomTypeCollection,
          serviceCollection: serviceCollection,
          mapCollection: mapCollection
        }))
        , defComp = result[0]
        ;
      defComp.preloader['bcCollection'] = bcCollection.toJSON();
      defComp.preloader['regionCollection'] = regionCollection.toJSON();
      defComp.preloader['metroCollection'] = metroCollection.toJSON();
      defComp.preloader['roomTypeCollection'] = roomTypeCollection.toJSON();
      defComp.preloader['serviceCollection'] = serviceCollection.toJSON();
      defComp['prime_cont'] = complSearch;
      res.render('index', _.extend(defComp, {meta: config.meta}));
    });
});

router.get('/bc/:id_or_url', function(req, res, next) {
  var BusinessCenterView = React.createFactory(require('components/BusinessCenter/BusinessCenterView'))
    , bcCollection = new BusinessCenterCollection()
    , regionCollection = new RegionCollection()
    , metroCollection = new MetroCollection()
    , roomTypeCollection = new RoomTypeCollection()
    , serviceCollection = new ServiceCollection()
    , storage = new Storage(req, res)
    , id_or_url = req.params['id_or_url']
    , id = BusinessCenterModel.getIdFromUrl(id_or_url)
    , bc = new BusinessCenterModel({id: id})
    ;
  promise.all(
    defaultComponents(req, res),
    bc.fetch(),
    regionCollection.fetch(),
    metroCollection.fetch(),
    roomTypeCollection.fetch(),
    serviceCollection.fetch()
    )
    .then(function(result) {
      if (!bc.get('full')) {
        next();
      }
      var view = ReactDOM.renderToString(BusinessCenterView({
          model: bc,
          storage: storage,
          regionCollection: regionCollection,
          metroCollection: metroCollection,
          roomTypeCollection: roomTypeCollection,
          serviceCollection: serviceCollection
        }))
        , defComp = result[0]
        , meta = {}
        ;
      bcCollection.add(bc);
      defComp.preloader['bcCollection'] = bcCollection.toJSON();
      defComp.preloader['regionCollection'] = regionCollection.toJSON();
      defComp.preloader['metroCollection'] = metroCollection.toJSON();
      defComp.preloader['roomTypeCollection'] = roomTypeCollection.toJSON();
      defComp.preloader['serviceCollection'] = serviceCollection.toJSON();
      defComp['prime_cont'] = view;

      meta.description = `Аренда офиса или помещения в бизнес центр ${bc.get('title')} — площади в БЦ ${bc.get('title')}, фото, описание, расположение. Продажа офисов в деловом центре ${bc.get('title')} .`;
      meta.keywords = `аренда офиса, аренда помещения, бизнес центр ${bc.get('title')}, продажа офисов в бц, деловой центр в санкт-петербурге, БЦ ${bc.get('title')}, бизнес-центр ${bc.get('title')}, аренда бц ${bc.get('title')}, бц БЦ ${bc.get('title')}, бизнес-центр ${bc.get('title')} спб, ставка бц БЦ ${bc.get('title')}, бизнес-центр ${bc.get('title')}`;

      res.render('view', _.extend(defComp, {meta: meta, bc: bc}));
    });
});

router.get('/favorites', function(req, res, next) {
  var Favorites = React.createFactory(require('components/Favorites'))
    , favoritesCollection = new BusinessCenterCollection()
    , regionCollection = new RegionCollection()
    , metroCollection = new MetroCollection()
    , roomTypeCollection = new RoomTypeCollection()
    , serviceCollection = new ServiceCollection()
    , storage = new Storage(req, res)
    , favoriteRooms = storage.get('favoriteRooms').length ? storage.get('favoriteRooms') : [0]
    , SearchModel = require('models/SearchModel')
    , searchModel = new SearchModel({'with_room_ids': favoriteRooms})
    ;
  promise.all(
    defaultComponents(req, res),
    favoritesCollection.fetch({data: searchModel.getData()}),
    regionCollection.fetch(),
    metroCollection.fetch(),
    roomTypeCollection.fetch(),
    serviceCollection.fetch()
    )
    .then(function(result) {
      var favorites = ReactDOM.renderToString(Favorites({
          collection: favoritesCollection,
          storage: storage,
          regionCollection: regionCollection,
          metroCollection: metroCollection,
          roomTypeCollection: roomTypeCollection,
          serviceCollection: serviceCollection
        }))
        , defComp = result[0]
        ;
      defComp.preloader['favoritesCollection'] = favoritesCollection.toJSON();
      defComp.preloader['regionCollection'] = regionCollection.toJSON();
      defComp.preloader['metroCollection'] = metroCollection.toJSON();
      defComp.preloader['roomTypeCollection'] = roomTypeCollection.toJSON();
      defComp.preloader['serviceCollection'] = serviceCollection.toJSON();
      defComp['prime_cont'] = favorites;
      res.render('index', _.extend(defComp, {meta: config.meta}));
    });
});



router.get('/:page_slug', function(req, res, next) {
  var PageModel = require('models/PageModel')
    , page_slug = req.params['page_slug']
    , id = PageModel.getIdFromUrl(page_slug)
    , page
      ;
  (!id || id == page_slug) && next();
  page = new PageModel({id: id});
  promise.all(
    defaultComponents(req, res),
    page.fetch()
    )
    .then(function(result) {
      var defComp = result[0];

      res.render('page', _.extend(defComp, {page: page}));
    });
});

module.exports = router;