var React = require('react')
  , Button = require('react-bootstrap/lib/Button')
  , Input = require('react-bootstrap/lib/Input')
  , Modal = require('react-bootstrap/lib/Modal')
  , Alert = require('react-bootstrap/lib/Alert')
  ;

module.exports = React.createClass({
  displayName: 'CallRequest',
  getInitialState: function() {
    return {
      showModal: false,
      isSent: false
    };
  },
  close: function() {
    this.setState({ showModal: false });
  },
  open: function() {
    this.setState({ showModal: true });
  },
  inputChange: function(e) {
    var st = {};
    st[e.target.name] = e.target.value;
    this.setState(st);
  },
  send: function() {
    this.setState({isSent: true});
  },
  render: function() {
    var body = this.state.isSent
        ? (
          <Alert bsStyle='success' style={{marginBottom: 0}}>
            <strong>Спасибо!</strong> Наш менеджер свяжется с Вами.
          </Alert>
        )
        : (
          <form onSubmi={function(e){e.preventDefault(); this.send();}.bind(this) }>
            <Input
              type='text'
              placeholder='Ваше имя'
              ref='input'
              name='name'
              onChange={this.inputChange} />
            <Input
              type='text'
              placeholder='Телефон или email'
              ref='input'
              name='email'
              onChange={this.inputChange} />
          </form>
        )
      , footer = this.state.isSent
        ? null
        : (
          <Modal.Footer>
            <Button onClick={this.send}>Заказать!</Button>
          </Modal.Footer>
        )
      , header = this.state.isSent
        ? null
        : (
          <Modal.Header closeButton>
            <Modal.Title>Заказать звонок</Modal.Title>
          </Modal.Header>
        )
        ;
    return (
      <div className='callRequest'>
        <a onClick={this.open} className={this.props.toggleClass}>{this.props.text || 'Заказать звонок'}</a>
        <Modal show={this.state.showModal} onHide={this.close} bsSize={this.state.isSent ? 'medium' : 'small'}>
          {header}
          <Modal.Body>
            {body}
          </Modal.Body>
          {footer}
        </Modal>
      </div>
    );
  }
});