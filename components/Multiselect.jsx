var React = require('react')
  , Dropdown = require('react-bootstrap/lib/Dropdown')
  , MenuItem = require('react-bootstrap/lib/MenuItem')
  , _ = require('underscore')
  , RootCloseWrapper = require('react-overlays/lib/RootCloseWrapper')
  ;

module.exports = React.createClass({
  displayName: 'Multiselect',
  getInitialState: function () {
    return {
      open: false
    };
  },
  getSelected: function() {
    return _.where(this.props.data, {selected: true});
  },
  onSelect: function(e, eventKey) {
    var newData = this.props.data.map(function(item) {
      return {
        value: item.value,
        label: item.label,
        selected: eventKey == item.value ? !item.selected : item.selected
      };
    });
    this.setState({
      data: newData
    });
    if (this.props.onChange) {
      this.props.onChange(newData);
    }
  },
  getTitle: function() {
    var selected = this.getSelected().length;
    return selected ? this.props.manyTitle+': ' + selected : this.props.oneTitle;
  },
  onToggle: function() {
    this.setState({
      open: !this.state.open
    });
  },
  render: function() {
    var title = this.getTitle()
      , items = this.props.data.map(function(item) {
        return (
          <MenuItem active={item.selected} onSelect={this.onSelect} key={item.value} eventKey={item.value}>
            {item.icon ? item.icon : ''}
            {item.label}
          </MenuItem>
        );
      }, this)
      , list = (
          <ul bsRole='menu' className='dropdown-menu'>
            {items}
          </ul>
      )
      , component = (
        <Dropdown id={this.props.id} onToggle={this.onToggle} className='multiselect' open={this.state.open}>
          <Dropdown.Toggle className={this.props.buttonClass}>{title}</Dropdown.Toggle>
          {list}
        </Dropdown>
      )
      ;
    if (this.state.open) {
      component = (
        <RootCloseWrapper noWrap onRootClose={this.onToggle}>
          {component}
        </RootCloseWrapper>
      );
    }
    return component;
  }
});