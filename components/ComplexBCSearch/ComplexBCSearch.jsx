var React = require('react')
  , ReactBackbone = require('react.backbone')
  , SearchBar = require('./SearchBar/SearchBar')
  , SearchMap = require('./SearchMap/SearchMap')
  , SearchResult = require('./SearchResult/SearchResult')
  , _ = require('underscore')
  ;

module.exports = React.createBackboneClass({
  displayName: 'ComplexBCSearch',
  getInitialState: function () {
    return {
      searchMapAll: this.props.storage.get('searchMap.bcAll'),
    };
  },
  fetchResults: function() {
    this.getCollection().fetch({reset: true, data: this.props.searchModel.getData()});
  },
  searchMapAllToggle: function() {
    var val = !this.state.searchMapAll;
    this.props.storage.set('searchMap.bcAll', val);
    this.setState({searchMapAll: val});
  },
  searchMapCollection: function() {
    return this.state.searchMapAll ? this.props.mapCollection : this.getCollection();
  },
  componentWillUnmount: function () {
    this.props.searchModel.off('change', _.debounce(this.fetchResults, 300));
  },
  componentDidMount: function() {
    this.props.searchModel.on('change', _.debounce(this.fetchResults, 300));
  },
  render: function() {
    return (
      <div id='complexBCSearch'>
        <SearchBar
          submit={this.fetchResults}
          model={this.props.searchModel}
          regionCollection={this.props.regionCollection}
          metroCollection={this.props.metroCollection}
          roomTypeCollection={this.props.roomTypeCollection}
          serviceCollection={this.props.serviceCollection} />
        <SearchMap
          collection={this.searchMapCollection()}
          storage={this.props.storage}
          bcAll={this.state.searchMapAll}
          metroCollection={this.props.metroCollection}
          bcAllToggle={this.searchMapAllToggle} />
        <div className='container'>
          <SearchResult
            collection={this.getCollection()}
            regionCollection={this.props.regionCollection}
            metroCollection={this.props.metroCollection}
            serviceCollection={this.props.serviceCollection}
            storage={this.props.storage} />
        </div>
      </div>
    );
  }
});