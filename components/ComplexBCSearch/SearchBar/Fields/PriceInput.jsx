var React = require('react')
  , ReactBackbone = require('react.backbone')
  , InputRange = require('components/InputRange')
  ;

module.exports = React.createBackboneClass({
  displayName: 'PriceInput',
  handleChange: function(data) {
    this.getModel().set({
      with_rooms_price_gt: data.min,
      with_rooms_price_lt: data.max
    });
  },
  render: function() {
    return (
      <InputRange
        id='footage-input'
        title='Цена'
        suffix=' руб'
        min={this.getModel().get('with_rooms_price_gt')}
        max={this.getModel().get('with_rooms_price_lt')}
        buttonClass='btn btn-default btn-sm'
        onChange={this.handleChange} />
    )
  }
});