var React = require('react')
  , ReactBackbone = require('react.backbone')
  , Multiselect = require('components/Multiselect')
  , _ = require('underscore')
  ;

module.exports = React.createBackboneClass({
  displayName: 'ServiceInput',
  handleChange: function(newData) {
    var selected = _.where(newData, {selected: true});
    this.getModel().set('with_service_id', _.pluck(selected, 'value'));
  },
  render: function() {
    var data = this.getCollection().map(function(service) {
      return {
        value: service.get('id'),
        label: service.get('title'),
        selected: _.contains(this.getModel().get('with_service_id') || [], service.get('id'))
        // ,
        // icon: (
        //   <span className={'icon icon-'+service.get('class_name')}> </span>
        // )
      };
    }.bind(this));

    return (
      <Multiselect
        id='service-input'
        oneTitle={'Услуги'}
        manyTitle={'Услуг'}
        onChange={this.handleChange}
        buttonClass='btn btn-default btn-sm'
        data={data} />
    )
  }
});