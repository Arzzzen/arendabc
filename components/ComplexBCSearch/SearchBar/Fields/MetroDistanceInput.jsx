var React = require('react')
  , ReactBackbone = require('react.backbone')
  ;

module.exports = React.createBackboneClass({
  displayName: 'MetroDistanceInput',
  handleChange: function(e) {
    this.getModel().set('with_metro_distance_st', e.target.value);
  },
  render: function() {
    return (
      <input
        className='form-control input-sm'
        placeholder='Минут до метро'
        value={this.getModel().get('with_metro_distance_st')}
        onChange={this.handleChange} />
    )
  }
});