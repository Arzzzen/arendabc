var React = require('react')
  , ReactBackbone = require('react.backbone')
  , InputRange = require('components/InputRange')
  ;

module.exports = React.createBackboneClass({
  displayName: 'AreaInput',
  handleChange: function(data) {
    this.getModel().set({
      with_rooms_area_gt: data.min,
      with_rooms_area_lt: data.max
    });
  },
  render: function() {
    return (
      <InputRange
        id='footage-input'
        title='Площадь'
        suffix=' м<sup>2</sup>'
        min={this.getModel().get('with_rooms_area_gt')}
        max={this.getModel().get('with_rooms_area_lt')}
        buttonClass='btn btn-default btn-sm'
        onChange={this.handleChange} />
    )
  }
});