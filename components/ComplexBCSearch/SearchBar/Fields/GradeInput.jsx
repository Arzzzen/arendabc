var React = require('react')
  , ReactBackbone = require('react.backbone')
  , Multiselect = require('components/Multiselect')
  , _ = require('underscore')
  ;

module.exports = React.createBackboneClass({
  displayName: 'GradeInput',
  handleChange: function(newData) {
    var selected = _.where(newData, {selected: true});
    this.getModel().set('with_grade', _.pluck(selected, 'value'));
  },
  render: function() {
    var grades = [
        {title: 'A', id: 1},
        {title: 'B', id: 2},
        {title: 'C', id: 3},
        {title: 'D', id: 4}
      ]
      , data = _.map(grades, function(grade) {
        return {
          value: grade.id,
          label: grade.title,
          selected: _.contains(this.getModel().get('with_grade') || [], grade.id)
        };
      }.bind(this))
      ;

    return (
      <Multiselect
        id='grade-input'
        oneTitle={'Класс'}
        manyTitle={'Классы'}
        onChange={this.handleChange}
        buttonClass='btn btn-default btn-sm'
        data={data}
      />
    )
  }
});