var React = require('react')
  , ReactBackbone = require('react.backbone')
  ;

module.exports = React.createBackboneClass({
  displayName: 'NameInput',
  handleChange: function(e) {
    this.getModel().set('search_query', e.target.value);
  },
  render: function() {
    return (
      <input
        className='form-control input-sm'
        placeholder='Название или улица'
        value={this.getModel().get('search_query')}
        onChange={this.handleChange} />
    )
  }
});