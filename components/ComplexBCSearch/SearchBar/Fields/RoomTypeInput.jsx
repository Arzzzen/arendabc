var React = require('react')
  , ReactBackbone = require('react.backbone')
  , Multiselect = require('components/Multiselect')
  , _ = require('underscore')
  ;

module.exports = React.createBackboneClass({
  displayName: 'RoomTypeInput',
  handleChange: function(newData) {
    var selected = _.where(newData, {selected: true});
    this.getModel().set('with_rooms_type', _.pluck(selected, 'value'));
  },
  render: function() {
    var data = this.getCollection().map(function(metro) {
      return {
        value: metro.get('id'),
        label: metro.get('title'),
        selected: _.contains(this.getModel().get('with_rooms_type') || [], metro.get('id'))
      };
    }.bind(this));

    return (
      <Multiselect
        id='metro-input'
        oneTitle={'Тип помещения'}
        manyTitle={'Типы помещения'}
        onChange={this.handleChange}
        buttonClass='btn btn-default btn-sm'
        data={data} />
    )
  }
});