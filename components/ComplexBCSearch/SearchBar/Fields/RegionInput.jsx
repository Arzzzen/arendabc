var React = require('react')
  , ReactBackbone = require('react.backbone')
  , Multiselect = require('components/Multiselect')
  , _ = require('underscore')
  ;

module.exports = React.createBackboneClass({
  displayName: 'RegionInput',
  handleChange: function(newData) {
    var selected = _.where(newData, {selected: true});
    this.getModel().set('with_region_id', _.pluck(selected, 'value'));
  },
  render: function() {
    var data = this.getCollection().map(function(region) {
      return {
        value: region.get('id'),
        label: region.get('title'),
        selected: _.contains(this.getModel().get('with_region_id') || [], region.get('id'))
      };
    }.bind(this));

    return (
      <Multiselect
        id='region-input'
        oneTitle={'Район'}
        manyTitle={'Районов'}
        onChange={this.handleChange}
        buttonClass='btn btn-default btn-sm'
        data={data}
      />
    )
  }
});