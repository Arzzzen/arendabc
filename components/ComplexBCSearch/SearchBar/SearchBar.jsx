var React = require('react')
  , ReactBackbone = require('react.backbone')
  , Button = require('react-bootstrap/lib/Button')
  , NameInput = require('components/ComplexBCSearch/SearchBar/Fields/NameInput')
  , RegionInput = require('components/ComplexBCSearch/SearchBar/Fields/RegionInput')
  , MetroInput = require('components/ComplexBCSearch/SearchBar/Fields/MetroInput')
  , AreaInput = require('components/ComplexBCSearch/SearchBar/Fields/AreaInput')
  , PriceInput = require('components/ComplexBCSearch/SearchBar/Fields/PriceInput')
  , Sticky = require('react-sticky')
  , MetroDistanceInput = require('components/ComplexBCSearch/SearchBar/Fields/MetroDistanceInput')
  , GradeInput = require('components/ComplexBCSearch/SearchBar/Fields/GradeInput')
  , RoomTypeInput = require('components/ComplexBCSearch/SearchBar/Fields/RoomTypeInput')
  , ServiceInput = require('components/ComplexBCSearch/SearchBar/Fields/ServiceInput')
  ;

module.exports = React.createBackboneClass({
  displayName: 'SearchBar',
  getInitialState: function() {
    return {
      advanced: false,
      mobileToggled: false
    };
  },
  handleStickyStateChange: function (isSticky) {
    // this.setState({
    //   advanced: false
    // });
  },
  toggleAdvanced: function() {
    this.setState({
      advanced: !this.state.advanced
    });
  },
  render: function() {
    var advanced
      , searchGroup = (
          <div className='field-2 text-right pull-right'>
            <Button bsSize='small' type='submit' title='Найти' className='pull-left'>
              <i className="fa fa-search"></i>
            </Button>
            <Button bsSize='small' className='btn-reset' title='Очистить поиск' onClick={this.getModel().clear.bind(this.getModel())}>
              <i className='fa fa-times'></i>
            </Button>&nbsp;
            <Button bsSize='small' type='submit' title={this.state.advanced ? 'Скрыть расширенный поиск' : 'Расширенный поиск'} className='pull-right' onClick={this.toggleAdvanced}>
              <span className={'icon icon-search-'+(this.state.advanced ? 'minus' : 'plus')}></span>
            </Button>
          </div>
        )
      , style = {
          maxHeight: this.state.mobileToggled ? document.documentElement.clientHeight : null
        }
      ;
    if (this.state.advanced) {
      advanced = (
        <div className="row">
            <div className='form-group field-2'>
              <MetroDistanceInput model={this.getModel()} />
            </div>
            <div className='form-group field-2'>
              <GradeInput model={this.getModel()} />
            </div>
            <div className='form-group field-2'>
              <RoomTypeInput model={this.getModel()} collection={this.props.roomTypeCollection} />
            </div>
            <div className='form-group field-2'>
              <ServiceInput model={this.getModel()} collection={this.props.serviceCollection} />
            </div>
            <div className='spacer visible-sm-block' />
            {searchGroup}
        </div>
      )
    }
    return (
      <Sticky onStickyStateChange={this.handleStickyStateChange}>
        <div id='searchBar' className={this.state.mobileToggled ? 'mobileToggled' : ''} style={style}>
          <form onSubmit={function(e) { e.preventDefault(); this.props.submit}.bind(this) } className='form-inline container'>
            <div className='row'>
              <div className={'form-group '+(this.state.advanced ? 'field-4' : 'field-2')}>
                <NameInput model={this.getModel()} />
              </div>
              <div className='form-group field-2'>
                <RegionInput model={this.getModel()} collection={this.props.regionCollection} />
              </div>
              <div className='form-group field-2'>
                <MetroInput model={this.getModel()} collection={this.props.metroCollection} />
              </div>
              { this.state.advanced ? <div className='spacer visible-sm-block' /> : null }
              <div className='form-group field-2'>
                <AreaInput model={this.getModel()} />
              </div>
              { !this.state.advanced ? <div className='spacer visible-sm-block' /> : null }
              <div className='form-group field-2'>
                <PriceInput model={this.getModel()} />
              </div>
              {this.state.advanced ? '' : searchGroup}
            </div>
            {advanced}
          </form>
          <div className='mobileToggle visible-xs-block' onClick={() => this.setState({mobileToggled: !this.state.mobileToggled})}>
            <i className='fa'></i>
          </div>
        </div>
      </Sticky>
    );
  }
});