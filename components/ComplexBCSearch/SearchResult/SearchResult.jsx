var React = require('react')
  , ReactBackbone = require('react.backbone')
  , BusinessCentersList = require('components/BusinessCenter/BusinessCentersList')
  , Paginator = require('components/Paginator')
  ;

module.exports = React.createBackboneClass({
  displayName: 'SearchResult',
  render: function() {
    return (
      <div id='searchResult'>
        <BusinessCentersList
          collection={this.getCollection()}
          regionCollection={this.props.regionCollection}
          metroCollection={this.props.metroCollection}
          storage={this.props.storage}
          serviceCollection={this.props.serviceCollection}
        />
        <Paginator collection={this.getCollection()} />
      </div>
    );
  }
});