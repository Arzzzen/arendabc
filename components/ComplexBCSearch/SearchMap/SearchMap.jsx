var React = require('react')
  , ReactBackbone = require('react.backbone')
  , GoogleMap = require('google-map-react/lib/google_map')
  , config = require('lib/Config')
  , Button = require('react-bootstrap/lib/Button')
  , BusinessCenterMarker = require('components/BusinessCenter/BusinessCenterMarker')
  ;

module.exports = React.createBackboneClass({
  displayName: 'SearchMap',
  getInitialState: function () {
    return {
      isVisible: this.props.storage.get('searchMap.isVisible')
    };
  },
  toggle: function() {
    var val = !this.state.isVisible;
    this.props.storage.set('searchMap.isVisible', val);
    this.setState({isVisible: val});
  },
  bcClick: function(id) {
    this.props.bcAll
      ? window.open(this.getCollection().find(id).getUrl(), 'blank')
      : window.scrollTo(0, document.querySelector('[data-id="'+id+'"]').offsetTop - ( window.innerHeight / 2 ))
      ;
  },
  render: function() {
    var markers = this.state.all
        ? null
        : this.getCollection()
          .filter(function(bc) {
            return parseFloat(bc.get('latitude')) && parseFloat(bc.get('longitude'));
          })
          .map(function(bc) {
            var bcLat = parseFloat(bc.get('latitude'))
              , bcLng = parseFloat(bc.get('longitude'))
              , color = bc.get('bc_metro_distances').length
                ? this.props.metroCollection.get(bc.get('bc_metro_distances')[0].metro_id).get('color')
                : null
                ;
            return <BusinessCenterMarker
                    model={bc}
                    key={bc.get('id')}
                    lat={bcLat}
                    lng={bcLng}
                    size={15}
                    styles={{color: color}}
                    onClick={function() { this.bcClick(bc.get('id')) }.bind(this) } />;
          }, this)
      , content = this.state.isVisible
        ? <div className='map-holder'>
            <GoogleMap
              defaultCenter={config.googleMap.defaultCenter}
              defaultZoom={config.googleMap.defaultZoom}>
              {markers}
            </GoogleMap>
            <Button
              bsSize='small'
              className='bc-all'
              onClick={this.props.bcAllToggle}><i className={'fa '+(this.props.bcAll ? 'fa-check-square-o' : 'fa-square-o')}></i> Все БЦ</Button>
            <Button
              bsSize='small'
              title={'Скрыть'}
              className='map-hide'
              onClick={this.toggle}>Скрыть</Button>
          </div>
        : <div className='map-placeholder'>
            <Button
              bsSize='small'
              title={'Показать карту'}
              className='map-show'
              onClick={this.toggle}>Показать карту</Button>
          </div>
        ;
    return (
          <div id='searchMap'>
            {content}
          </div>
        );
  }
});