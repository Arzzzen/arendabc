var React = require('react')
  , ReactBackbone = require('react.backbone')
  , BusinessCentersList = require('components/BusinessCenter/BusinessCentersList')
    ;

module.exports = React.createBackboneClass({
  displayName: 'Favorites',
  render: function() {
    return (
      <div className="favorites">
        <div className='page-head'>
          <div className='container'>
            <h1 className='page-title'>Избранное</h1>
          </div>
        </div>
        <div className='container'>
          <BusinessCentersList
            collection={this.getCollection()}
            regionCollection={this.props.regionCollection}
            metroCollection={this.props.metroCollection}
            storage={this.props.storage}
            serviceCollection={this.props.serviceCollection}
          />
        </div>
      </div>
    );
  }
});