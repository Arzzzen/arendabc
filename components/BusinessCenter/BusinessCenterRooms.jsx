var React = require('react')
  , ReactBackbone = require('react.backbone')
  , _ = require('underscore')
  , formatNumber = require('format-number')(require('lib/Config')['numberFormat'])
  , formatPrice = require('format-number')(require('lib/Config')['priceFormat'])
  ;

var Room = React.createClass({
  isFavorite: function() {
    var favoriteRooms = this.props.storage.get('favoriteRooms');
    return _.isArray(favoriteRooms) && favoriteRooms.indexOf(this.props.room.id) >= 0;
  },
  getInitialState: function() {
    return {
      favorite: this.isFavorite()
    };
  },
  toggleFavorites: function() {
    var favorite = this.isFavorite()
      , favoriteRooms = this.props.storage.get('favoriteRooms') || []
      ;
    if (favorite) {
      favoriteRooms = _.without(favoriteRooms, this.props.room.id);
    } else {
      favoriteRooms.push(this.props.room.id);
    }
    this.props.storage.set('favoriteRooms', favoriteRooms)
    this.setState({
      favorite: !favorite
    });
  },
  render: function() {
    var room = this.props.room
      , favoriteClass = this.state.favorite ? 'fa fa-bookmark' : 'fa fa-bookmark-o'
      ;
    return (
      <tr onClick={this.toggleFavorites}>
        <td>{room.type ? room.type : '-'}</td>
        <td>{room.floor ? room.floor : '-'}</td>
        <td>{room.area ? formatNumber(room.area) : '-'}</td>
        <td>{room.price ? formatPrice(room.price) : '-'}</td>
        <td>
          <i className={favoriteClass}></i>
        </td>
      </tr>
    );
  }
});

module.exports = React.createBackboneClass({
  toggle: function() {
    this.props.toggle();
  },
  render: function() {
    var roomsAr = this.getModel().get('rooms') || [];
    // roomsAr = _.sortBy(roomsAr, (room) => !_.indexOf((this.props.storage.get('favoriteRooms') || []), room.id) );
    if (!roomsAr.length) {
      return (
        <small className='text-muted'>Уточняйте свободные площади по телефону</small>
      );
    }
    var manyRooms = roomsAr.length > this.props.showContracted
      , showContracted = !this.props.expanded && manyRooms
      , showRooms = showContracted
          ? _.first(roomsAr, this.props.showContracted-2).concat( _.last(roomsAr) )
          : roomsAr
      , rooms = []
      , toggle = (
          <span className='btn btn-default btn-xs toggle' onClick={showContracted ? function(){} : this.toggle}>
            {this.props.expanded ? 'скрыть' : 'все площади'}
          </span>
        )
      ;
    _.each(showRooms, function(room, i) {
      if (showContracted && showRooms.length-1 == i) {
        rooms.push( (
          <tr key='toggle' onClick={this.toggle}>
            <td colSpan='5' className='delimeter'>
              {toggle}
            </td>
          </tr>
        ) );
      }

      rooms.push( (
        <Room
          room={room}
          key={room.id}
          storage={this.props.storage} />
      ) );
    }, this);
    return (
      <div className={'businessCenterRooms'+(manyRooms && !showContracted ? ' expanded' : '')}>
        <table>
          <thead>
            <tr>
              <th>Тип</th>
              <th>Этаж</th>
              <th>Метраж</th>
              <th>р/м<sup>2</sup></th>
              <th><i className='fa fa-star'></i></th>
            </tr>
          </thead>
          <tbody>
            {rooms}
          </tbody>
        </table>
        {manyRooms && !showContracted ? toggle : ''}
      </div>
    );
  }
});