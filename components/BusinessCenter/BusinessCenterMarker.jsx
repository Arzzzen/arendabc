var React = require('react')
  , ReactBackbone = require('react.backbone')
  , _ = require('underscore')
  ;

module.exports = React.createBackboneClass({
  defaultStyles: function() {
    return {
      width: this.props.size,
      height: this.props.size,
      left: -this.props.size / 2,
      top: -this.props.size / 2,
      fontSize: this.props.size+'px'
    };
  },
  render: function() {
    var markerStyle = _.extend(this.defaultStyles(), this.props.styles)
      , elem = this.props.$hover
          ? <div>{this.getModel().get('title')} <div className='address'>{this.getModel().get('address')}</div></div>
          : <span className='icon icon-pin'></span>
        ;
    return (
      <div className={'bcMarker'+(this.props.$hover ? ' hover' : '')} style={this.props.$hover ? {} : markerStyle} onClick={this.props.onClick}>
        {elem}
      </div>
    );
  }
});