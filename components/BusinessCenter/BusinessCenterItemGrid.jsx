var React = require('react')
  , ReactBackbone = require('react.backbone')
  , Phone = require('components/BusinessCenter/View/Phone')
  ;

module.exports = React.createBackboneClass({
  render: function() {
    var image = this.getModel().get('images')[0]
      , bcLink = this.getModel().getUrl()
      , regions = this.getModel().get('regions').map(function (region_id) {
          var region = this.props.regionCollection.get(region_id);
          if (region) {
            return <span className='label' key={region.get('id')}>{region.get('title')}</span>;
          }
        }, this)
      , metros = this.getModel().get('bc_metro_distances').map(function (bc_metro_distance) {
          var metro = this.props.metroCollection.get(bc_metro_distance.metro_id)
            , distance = bc_metro_distance.distance
              ? (<span className="dist">({bc_metro_distance.distance} мин)</span>)
              : null;
            ;
          if (metro) {
            return (
              <div key={metro.get('id')} style={{color: metro.get('color')}}>
                <i className="fa fa-subway"></i> {metro.get('title')} {distance}
              </div>
            );
          }
        }, this)
        ;
    return (
      <div className='bcItemGrid'>
        <div className='info'>
          <div className='img'>
            <img src={image} />
            <span className='label grade-label'>{this.getModel().get('grade')}</span>
            <Phone model={this.getModel()} />
          </div>
          <div className="cont">
            <a href={bcLink} className='title' target='_blank'>{this.getModel().get('title')}</a>
            <div className='address'>{this.getModel().get('address')}</div>
            <div className="regions">
              {regions}
            </div>
            <div className="metros">
              {metros}
            </div>
          </div>
        </div>
      </div>
    );
  }
});