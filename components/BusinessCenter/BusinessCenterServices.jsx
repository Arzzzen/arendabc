var React = require('react')
  , ReactBackbone = require('react.backbone')
  , _ = require('underscore')
  ;

module.exports = React.createBackboneClass({
  showContracted: 3,
  render: function() {
    var servicesAr = this.getModel().get('business_center_service') || []
      , showServices = this.props.expanded ? servicesAr : _.first(servicesAr, this.showContracted)
      , services = _.map(showServices, function (bc_service) {
        var service = this.props.serviceCollection.get(bc_service.service_id);
        if (!service) {
          return;
        }
        return (
          <span key={service.get('id')} className={'service icon-'+service.get('class_name')}>
            <div className='title'>
              {service.get('title')}
            </div>
            <div className="desc">{bc_service.desc}</div>
          </span>
        )
      }, this)
      ;
    return (
      <div className='businessCenterServices'>
        {services}
      </div>
    );
  }
});