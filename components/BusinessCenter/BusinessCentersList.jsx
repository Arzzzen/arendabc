var React = require('react')
  , ReactBackbone = require('react.backbone')
  , BusinessCenterItem = require('components/BusinessCenter/BusinessCenterItem')
  ;

module.exports = React.createBackboneClass({
  render: function() {
    var items = this.getCollection().map(function(bc) {
      return (
        <BusinessCenterItem
          model={bc}
          key={bc.get('id')}
          regionCollection={this.props.regionCollection}
          metroCollection={this.props.metroCollection}
          serviceCollection={this.props.serviceCollection}
          storage={this.props.storage} />
      );
    }, this);
    return (
      <div className='bcs_list'>
        {function() {
          return items.length
            ? items
            : <p>По заданным критериям не найдено ни одного свободного помещения</p>
        }()}
      </div>
    );
  }
});