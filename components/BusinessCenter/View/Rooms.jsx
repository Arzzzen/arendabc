var React = require('react')
  , ReactBackbone = require('react.backbone')
  , BusinessCenterRooms = require('components/BusinessCenter/BusinessCenterRooms')
  , CallRequest = require('components/CallRequest')
  ;

var Requests = React.createBackboneClass({
  render: function() {
    return (
      <div className='portlet requests box blue-hoki'>
        <div className='portlet-title'>
          Тел: <span className='phone'>{this.getModel().get('phone')}</span>
        </div>
        <div className='portlet-body'>
          <CallRequest toggleClass='btn btn-primary' text='Оставить заявку на просмотр' />
          <CallRequest toggleClass='btn btn-primary' text='Запросить планировку избранных площадей' />
        </div>
      </div>
    );
  }
});

module.exports = React.createBackboneClass({
  displayName: 'Rooms',
  getInitialState: function() {
    return {
      expanded: false
    };
  },
  toggle: function() {
    this.setState({
      expanded: !this.state.expanded
    });
  },
  render: function() {
    return (
      <div className='bcRooms'>
        <div className='container'>
          <div className='portlet box red'>
            <div className='portlet-title'>
              <div className='caption'>Свободные площади</div>
            </div>
            <div className='portlet-body'>
              <div className="row">
                <div className="col-md-7">
                  <BusinessCenterRooms
                    showContracted={10}
                    model={this.getModel()}
                    storage={this.props.storage}
                    toggle={this.toggle}
                    expanded={this.state.expanded} />
                </div>
                <div className="col-md-5">
                  <Requests model={this.getModel()} />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
});