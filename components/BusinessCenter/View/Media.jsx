var React = require('react')
  , ReactBackbone = require('react.backbone')
  , Nav = require('react-bootstrap/lib/Nav')
  , NavItem = require('react-bootstrap/lib/NavItem')
  , MediaPhoto = require('components/BusinessCenter/View/MediaPhoto')
  , MediaMap = require('components/BusinessCenter/View/MediaMap')
  ;

module.exports = React.createBackboneClass({
  getInitialState: function () {
    return {
      activeKey: 1
    };
  },
  handleSelect: function (selectedKey) {
    this.setState({
      activeKey: selectedKey
    });
  },
  togglePanorama: function() {
    this.setState({activeKey: this.state.activeKey == 2 ? 3 : 2});
  },
  render: function() {    
    return (
      <div className='bcMedia'>
        <div className="container">
          <Nav bsStyle='tabs' activeKey={this.state.activeKey} onSelect={this.handleSelect}>
            <NavItem eventKey={1}>Фото</NavItem>
            <NavItem eventKey={2}>Карта</NavItem>
            <NavItem eventKey={3}>Панорама улицы</NavItem>
          </Nav>
        </div>
        <div className="tabs-content">
          <MediaPhoto isHidden={this.state.activeKey == 1} model={this.getModel()} />
          <MediaMap
            isHidden={this.state.activeKey != 2 && this.state.activeKey != 3}
            model={this.getModel()}
            panorama={this.state.activeKey == 3}
            togglePanorama={this.togglePanorama} />
        </div>
      </div>
    );
  }
});