var React = require('react')
  , ReactBackbone = require('react.backbone')
  ;

module.exports = React.createBackboneClass({
  render: function() {
    return (
      <div className='page-head'>
        <div className='container'>
          <h1 className='page-title'>
            {this.getModel().get('title')} <small>{this.getModel().get('address')}</small>
          </h1>
          <div className="page-toolbar">
            <span className='label grade-label'>{this.getModel().get('grade')}</span>
          </div>
        </div>
      </div>
    );
  }
});