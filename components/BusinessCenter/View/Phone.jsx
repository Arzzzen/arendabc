var React = require('react')
  , ReactBackbone = require('react.backbone')
  ;

module.exports = React.createBackboneClass({
  getInitialState: function() {
    return {
      hidden: true
    };
  },
  changeVisibility: function(e) {
    if (this.state.hidden) {
      this.setState({hidden: false});
      e.preventDefault();
    }
  },
  render: function() {
    return (
      <a href={'tel:'+this.getModel().get('phone')} className='phone' onClick={this.changeVisibility}>
        <i className='fa fa-phone'></i> <span>{this.state.hidden ? this.getModel().get('phone').slice(0, 5)+'...' : this.getModel().get('phone')}</span>
      </a>
    );
  } 
});