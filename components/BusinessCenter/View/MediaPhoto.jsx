var React = require('react')
  , ReactBackbone = require('react.backbone')
  , Slider = require('react-slick')
  ;

module.exports = React.createBackboneClass({
  render: function() {
    var items = []
      , settings = {
          variableWidth: true,
          dots: true,
          infinite: true,
          adaptiveHeight: false,
          responsive: [
            {
              breakpoint: 768,
              settings: { slidesToShow: 1 }
            }
          ]
        }
        ;
    if (this.getModel().get('full')) {
      items = this.getModel().get('images_full').map(function (image_src, i) {
        return (
          <img src={image_src} key={i} />
        )
      });
    }
    return !this.props.isHidden
        ? null
        : (
          <Slider className='mediaPhoto' {...settings}>
            {items}
          </Slider>
        );
  }
});