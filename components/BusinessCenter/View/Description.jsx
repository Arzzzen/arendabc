var React = require('react')
  , ReactBackbone = require('react.backbone')
  ;

module.exports = React.createBackboneClass({
  render: function() {
    if (!this.getModel().get('description')) {
      return null;
    }
    return (
      <div className='bcDescription'>
        <div className='container'>
          <div className='portlet box brand'>
            <div className='portlet-title'>
              <div className='caption'>Описание</div>
            </div>
            <div className='portlet-body' dangerouslySetInnerHTML={{__html: this.getModel().get('description')}}></div>
          </div>
        </div>
      </div>
    );
  }
});