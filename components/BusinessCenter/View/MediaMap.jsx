var React = require('react')
  , ReactBackbone = require('react.backbone')
  , GoogleMap = require('google-map-react/lib/google_map')
  , BusinessCenterMarker = require('components/BusinessCenter/BusinessCenterMarker')
  ;

module.exports = React.createBackboneClass({
  mapOptions: function() {
    return {
      // streetViewControl: true
    };
  },
  render: function() {
    if (this.props.isHidden) {
      return null;
    }
    var bcLat = parseFloat(this.getModel().get('latitude'))
      , bcLng = parseFloat(this.getModel().get('longitude'))
      , defaultCenter = {
          lat: 59.938806,
          lng: 30.381225
        }
      , defaultZoom = 11
      , center = defaultCenter
      , zoom = defaultZoom
      , marker
        ;
    if (bcLat && bcLng) {
      zoom = 15;
      center = {
        lat: bcLat,
        lng: bcLng
      };
      marker = (
        <BusinessCenterMarker model={this.getModel()} lat={bcLat} lng={bcLng} size={20} />
      )
    }
    setTimeout(function() {
      if (!this._googleMapComponent || !this._googleMapComponent.map_) {
        return;
      }

      var map = this._googleMapComponent.map_
        , SV = map.getStreetView()
        , request = {
            location: center, 
            radius: 200, 
            types: ['gas_station','atm','hospital','restaurant',] 
          }
        , infowindow = new google.maps.InfoWindow()
        , service = new google.maps.places.PlacesService(map); 
          ;
      if (this.props.panorama) {
        SV.setVisible(this.props.panorama);
        SV.setPosition(center);
      } else {
        SV.setVisible(false);
        service.nearbySearch(request, callback);
        function callback(results, status) {
          if (status === google.maps.places.PlacesServiceStatus.OK) {
            for (var i = 0; i < results.length; i++) {
              createMarker(results[i]);
            }
          }
        }

        function createMarker(place) {
          var placeLoc = place.geometry.location
            , image = {
                url: place.icon,
                size: new google.maps.Size(71, 71),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(25, 25)
              }
            , marker = new google.maps.Marker({
                icon: image,
                map: map,
                position: place.geometry.location
              })
              ;

          google.maps.event.addListener(marker, 'click', function() {
            infowindow.setContent(place.name);
            infowindow.open(map, this);
          });
        }
      }
      
    }.bind(this), 100);
    if (this._googleMapComponent && this._googleMapComponent.map_) {
        ;
    }
    return (
          <div className='mediaMap'>
            <GoogleMap
              ref={it => this._googleMapComponent = it}
              bootstrapURLKeys={{libraries: 'places'}}
              options={this.mapOptions}
              center={center}
              zoom={zoom}
              >
              {marker}
            </GoogleMap>
          </div>
          );
  }
});