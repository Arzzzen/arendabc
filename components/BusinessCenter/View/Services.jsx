var React = require('react')
  , ReactBackbone = require('react.backbone')
  , BusinessCenterServices = require('components/BusinessCenter/BusinessCenterServices')
  ;

module.exports = React.createBackboneClass({
  render: function() {
    if (!this.getModel().get('business_center_service').length) {
      return null;
    }
    return (
      <div className='bcServices'>
        <div className='container'>
          <div className='portlet box purple-plum'>
            <div className='portlet-title'>
              <div className='caption'>Услуги</div>
            </div>
            <div className='portlet-body'>
              <BusinessCenterServices
                model={this.getModel()}
                expanded={true}
                serviceCollection={this.props.serviceCollection} />
            </div>
          </div>
        </div>
      </div>
    );
  }
});