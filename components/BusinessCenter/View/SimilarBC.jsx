var React = require('react')
  , ReactBackbone = require('react.backbone')
  , Nav = require('react-bootstrap/lib/Nav')
  , NavItem = require('react-bootstrap/lib/NavItem')
  , BusinessCenterItemGrid = require('components/BusinessCenter/BusinessCenterItemGrid')
  , BusinessCenterCollection = require('models/BusinessCenterCollection')
  , _ = require('underscore')
  ;

module.exports = React.createBackboneClass({
  displayName: 'SimilarBC',
  availableKeys: {
    1: {t: 'По метро'},
    2: {t: 'По району'},
    3: {t: 'По цене'}
  },
  getInitialState: function() {
    return {
      bcCollection: new BusinessCenterCollection(),
      activeKey: 1,
      loading: false
    }
  },
  handleSelect: function(selectedKey) {
    this.setState({
      activeKey: selectedKey
    }, () => this.fetchCollection());
  },
  fetchCollection: function() {
    var cond = {s: this.availableKeys[this.state.activeKey]['cond']};
    this.setState({ loading: true });
    this.state.bcCollection.fetch({ reset: true, data: _.extend(cond, {per_page: 4}) });
  },
  setAvailableKeys: function() {
    var bc = this.getModel();
    if (!_.isEmpty(bc.get('bc_metro_distances')) && bc.get('bc_metro_distances')[0]['metro_id']) {
      this.availableKeys[1]['cond'] = {with_metro_id: [bc.get('bc_metro_distances')[0]['metro_id']]};
    }
    if (!_.isEmpty(bc.get('regions')) && bc.get('regions')[0]['id']) {
      this.availableKeys[2]['cond'] = {with_region_id: [bc.get('regions')[0]['id']]};
    }
    if (!_.isEmpty(bc.get('rooms'))) {
      var max = _.max(bc.get('rooms'), (room) => room.price)
        , min = _.min(bc.get('rooms'), (room) => room.price)
        , cond = {}
          ;
      if (_.isNumber(min.price)) {
        cond['with_rooms_price_gt'] = min.price;
      }
      if (_.isNumber(max.price)) {
        cond['with_rooms_price_lt'] = max.price;
      }
      if (_.keys(cond).length) {
        this.availableKeys[3]['cond'] = cond;
      }
    }
  },
  componentWillMount: function() {
    this.setAvailableKeys();
  },
  componentDidMount: function() {
    this.fetchCollection();
    this.state.bcCollection.on('sync', function() {
      this.setState({
        bcCollection: this.state.bcCollection,
        loading: false
      });
    }.bind(this));
  },
  render: function() {
    if (!this.state.bcCollection.length || !_.pluck(_.values(this.availableKeys), 'cond').length) {
      return null;
    }
    var items = this.state.bcCollection.map(function(bc) {
          return (
            <BusinessCenterItemGrid
              model={bc}
              regionCollection={this.props.regionCollection}
              metroCollection={this.props.metroCollection}
              key={bc.get('id')} />
          );
        }, this)
        ;
    return (
      <div className='bcSimilarBC'>
        <div className='container'>
          <div className='portlet light blue-hoki'>
            <div className='portlet-title'>
              <div className='caption'>Похожие бизнес центры</div>
              <Nav bsStyle='tabs' activeKey={this.state.activeKey} onSelect={this.handleSelect}>
                <NavItem eventKey={1} disabled={!this.availableKeys[1]['cond']}>{this.availableKeys[1].t}</NavItem>
                <NavItem eventKey={2} disabled={!this.availableKeys[2]['cond']}>{this.availableKeys[2].t}</NavItem>
                <NavItem eventKey={3} disabled={!this.availableKeys[3]['cond']}>{this.availableKeys[3].t}</NavItem>
              </Nav>
            </div>
            <div className='portlet-body' style={{opacity: this.state.loading ? 0 : 1}}>
              <div className="row">
                {items}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
});