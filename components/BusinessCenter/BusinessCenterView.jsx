var React = require('react')
  , ReactBackbone = require('react.backbone')
  , Title = require('components/BusinessCenter/View/Title')
  , Media = require('components/BusinessCenter/View/Media')
  , Info = require('components/BusinessCenter/View/Info')
  , Rooms = require('components/BusinessCenter/View/Rooms')
  , Services = require('components/BusinessCenter/View/Services')
  , Description = require('components/BusinessCenter/View/Description')
  , SimilarBC = require('components/BusinessCenter/View/SimilarBC')
  ;

module.exports = React.createBackboneClass({
  displayName: 'BusinessCenterView',
  render: function() {
    return (
      <div id='BCView'>
        <Title model={this.getModel()} />
        <Media model={this.getModel()} />
        <Info
          model={this.getModel()}
          regionCollection={this.props.regionCollection}
          metroCollection={this.props.metroCollection} />
        <Rooms
          model={this.getModel()}
          storage={this.props.storage} />
        <Services
          model={this.getModel()}
          serviceCollection={this.props.serviceCollection} />
        <Description
          model={this.getModel()} />
        <SimilarBC
          model={this.getModel()}
          regionCollection={this.props.regionCollection}
          metroCollection={this.props.metroCollection} />
      </div>
    );
  }
});