var React = require('react')
  , ReactBackbone = require('react.backbone')
  , BusinessCentersList = require('components/BusinessCenter/BusinessCentersList')
    ;

module.exports = React.createBackboneClass({
  displayName: 'FavoritesMini',
  getInitialState: function() {
    return {
      roomNumber: (this.props.storage.get('favoriteRooms') || []).length
    };
  },
  hnadleFavoritesChange: function() {
    this.setState({
      roomNumber: (this.props.storage.get('favoriteRooms') || []).length
    });
  },
  componentWillUnmount: function() {
    this.props.storage.off('change:favoriteRooms', this.hnadleFavoritesChange);
  },
  componentDidMount: function() {
    this.props.storage.on('change:favoriteRooms', this.hnadleFavoritesChange);
  },
  render: function() {
    return <a href='/favorites' data-route className={'favorites-mini '+(this.state.roomNumber ? 'active' : '')} disabled={!!this.state.roomNumber}><span>{this.state.roomNumber}</span></a>;
  }
});