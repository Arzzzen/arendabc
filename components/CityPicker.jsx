var React = require('react')
  , DropdownButton = require('react-bootstrap/lib/DropdownButton')
  , MenuItem = require('react-bootstrap/lib/MenuItem');

module.exports = React.createClass({
  displayName: 'CityPicker',
  getInitialState: function() {
    return {
      active_city_id: this.props.searchModel.get('city')
    };
  },
  citySelect: function(ev, evKey) {
    this.setState({'active_city_id': evKey});
    this.props.searchModel.set('city', evKey);
  },
  render: function() {
    var active_city = this.props.cities[this.state.active_city_id ? 'get' : 'first'](this.state.active_city_id)
      , items = this.props.cities.map(function(city) {
        return (
          <MenuItem key={city.get('id')} eventKey={city.get('id')} active={active_city.get('id') == city.get('id')}>
            {city.get('title')}
          </MenuItem>
        );
      });

    return (
      <DropdownButton onSelect={this.citySelect} title={active_city.get('title')} id='city-drop' pullRight>
        {items}
      </DropdownButton>
    );
  }
});