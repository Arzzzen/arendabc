var React = require('react')
  , ReactBackbone = require('react.backbone')
  , _ = require('underscore')
  ;

module.exports = React.createBackboneClass({
  displayName: 'Paginator',
  getInitialState: function () {
    var per_page = 5;
    return {
      page: Math.ceil(this.getCollection().length / per_page),
      per_page: per_page,
      lastLoadedNum: this.getCollection().length,
      loading: false
    };
  },
  onSync: function(collection, data) {
    setTimeout(function() {
      this.setState({
        page: Math.ceil(this.getCollection().length / this.state.per_page),
        lastLoadedNum: data.length,
        loading: false
      });
    }.bind(this), 500);
  },
  loadMore: function() {
    var page = this.state.page+1;
    this.setState({
      page: page,
      loading: true
    });
    this.getCollection().fetch({
     remove: false,
     data: {page: page}
    });
  },
  componentWillUnmount: function () {
    this.getCollection().off('sync', this.omSync);
  },
  componentDidMount: function() {
    this.getCollection().on('sync', this.onSync);
  },
  render: function() {
    if (!this.getCollection().length) {
      return null;
    }
    var elem_text = this.state.loading
        ? <span className='icon icon-spinner'></span>
        : <span>Показать еще</span>
      , elem = this.state.lastLoadedNum >= this.state.per_page
        ? (
          <span className='btn btn-primary btn-block btn-lg' type='button' onClick={this.loadMore}>
            {elem_text}
          </span>
          )
        : (
          <p>Вы просмотрели все варианты</p>
          )
        ;
    
    return (
      <div id='paginator'>
        {elem}
      </div>
    );
  }
});