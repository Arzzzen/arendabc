var React = require('react')
  , Dropdown = require('react-bootstrap/lib/Dropdown')
  , RootCloseWrapper = require('react-overlays/lib/RootCloseWrapper')
  , Button = require('react-bootstrap/lib/Button')
  , format = require('format-number')(require('lib/Config')['numberFormat'])
  ;

module.exports = React.createClass({
  displayName: 'InputRange',
  getInitialState: function () {
    return {
      min: this.props.min,
      max: this.props.max,
      open: false
    };
  },
  handleChange: function(data) {
    if (this.props.onChange) {
      this.props.onChange(data);
    }
  },
  minChange: function(e) {
    var min = e.target.value;
    this.setState({
      min: min
    });
    this.handleChange({
      min: min,
      max: this.state.max
    });
  },
  maxChange: function(e) {
    var max = e.target.value;
    this.setState({
      max: max
    });
    this.handleChange({
      min: this.state.min,
      max: max
    });
  },
  getTitle: function() {
    var isMin = parseInt(this.state.min) == this.state.min
      , isMax = parseInt(this.state.max) == this.state.max
      , suffix = this.props.suffix || ''
      , minFormated = format(this.state.min)+suffix
      , maxFormated = format(this.state.max)+suffix
      , title = this.props.title
      , delimeter = this.props.delimeter || ' ... '
      ;
    if (isMin) {
      title = 'от '+minFormated;
    }
    if (isMax) {
      title = 'до '+maxFormated;
    }
    if (isMin && isMax) {
      title = minFormated+delimeter+maxFormated;
    }
    return title;
  },
  onToggle: function() {
    this.setState({
      open: !this.state.open
    });
  },
  onClear: function() {
    var nulls = {min: null, max: null};
    this.setState(nulls);
    this.handleChange(nulls);
    this.onToggle();
  },
  render: function() {
    var title = this.getTitle()
      , inputs = (
          <div bsRole='menu' className='dropdown-menu'>
            <input
              className='form-control input-sm'
              placeholder='Min'
              value={this.state.min}
              onSelect={function(e){e.stopPropagation()}}
              onChange={this.minChange} />
            <span className='dash-divider'>-</span>
            <input
              className='form-control input-sm'
              placeholder='Max'
              value={this.state.max}
              onSelect={function(e){e.stopPropagation()}}
              onChange={this.maxChange} />
            <Button bsSize='small' className='btn-clear' onClick={this.onClear}><i className='fa fa-times'></i></Button>
            <Button bsSize='small' className='pull-right' onClick={this.onToggle}>ОК</Button>
          </div>
      )
      , component = (
        <Dropdown id={this.props.id} onToggle={this.onToggle} className='input-range' open={this.state.open}>
          <Dropdown.Toggle className={this.props.buttonClass}>
            <span dangerouslySetInnerHTML={{__html:title}}></span>
          </Dropdown.Toggle>
          {inputs}
        </Dropdown>
      )
      ;
    if (this.state.open) {
      component = (
        <RootCloseWrapper noWrap onRootClose={this.onToggle}>
          {component}
        </RootCloseWrapper>
      );
    }
    return component;
  }
});