var React = require('react')
  , ReactDOM = require('react-dom')
  , Backbone = require('backbone')
  , Router = require('assets/js/Router')
  , BCController = require('assets/js/controllers/BCController')
  , $ = require('jquery')
  , Storage = require('lib/Storage/Storage')
  , UrlRouter = require('router')
  , qs = require('query-string-object')
  ;

var Application = function() {
  this.initialize();
};
Application.prototype.initialize = function() {
  this.controllers = {
    bc: new BCController({app: this})
  }
  this.router = new Router({
    controllers: this.controllers
  });
  this.storage = new Storage();
  this.preloader = window.preloader;

  this.initModels();
  this.initAppLinks();
}

Application.prototype.run = function() {
  this.showDefaultComponents();
  Backbone.history.start({pushState: true});
}

Application.prototype.showDefaultComponents = function() {
  var CityPicker = require('components/CityPicker');
  ReactDOM.render(
    <CityPicker cities={this.collections.cityCollection} searchModel={this.models.search} />,
    document.getElementById('city-picker')
  );

  var CallRequest = require('components/CallRequest');
  ReactDOM.render(
    <CallRequest />,
    document.getElementById('call-request')
  );

  var FavoritesMini = require('components/FavoritesMini');
  ReactDOM.render(
    <FavoritesMini storage={this.storage} />,
    document.getElementById('favorites-mini')
  );
}

Application.prototype.initAppLinks = function() {
  $('body').on('click', 'a[data-route]', function (e) {
    e.preventDefault();
    this.router.navigate(e.target.getAttribute('href'), {trigger: true});
    window.scrollTo(0, 0);
  }.bind(this));
}

Application.prototype.initModels = function() {
  var BusinessCenterCollection = require('models/BusinessCenterCollection')
    , SearchModel = require('models/SearchModel')
    , CityCollection = require('models/CityCollection')
    , RegionCollection = require('models/RegionCollection')
    , MetroCollection = require('models/MetroCollection')
    , RoomTypeCollection = require('models/RoomTypeCollection')
    , ServiceCollection = require('models/ServiceCollection')
    ;


  this.models = [];
  this.models.search = new SearchModel(SearchModel.getQueryParameters()['s']);

  this.models.search.on('change', function() {
    this.router.navigate(this.models.search.getUrl(), {trigger: true});
  }, this);

  this.collections = [];
  this.collections.bcCollection = new BusinessCenterCollection(this.preloader['bcCollection']);
  this.collections.cityCollection = new CityCollection(this.preloader['cityCollection']);
  this.collections.regionCollection = new RegionCollection(this.preloader['regionCollection']);
  this.collections.metroCollection = new MetroCollection(this.preloader['metroCollection']);
  this.collections.roomTypeCollection = new RoomTypeCollection(this.preloader['roomTypeCollection']);
  this.collections.serviceCollection = new ServiceCollection(this.preloader['serviceCollection']);
  this.collections.mapCollection = new BusinessCenterCollection(this.preloader['mapCollection']);
  this.collections.mapCollection.url = UrlRouter.getApiUrl('api/business_centers_for_map');
  this.collections.favoritesCollection = new BusinessCenterCollection(this.preloader['favoritesCollection']);
  this.storage.on('change:favoriteRooms', (fC) => this.collections.favoritesCollection = fC);
}

module.exports = Application;