document.addEventListener("DOMContentLoaded", function() {
  var Application = require('assets/js/Application');
  window.app = new Application();
  app.run();
});

document.addEventListener('DOMContentLoaded', () => {
  var offset = 100;


  if (navigator.userAgent.match(/iPhone|iPad|iPod/i)) {  // ios supported
    window.addEventListener('touchend touchcancel touchleave', function(e){
      document.querySelector('.scroll-to-top').style.opacity = window.pageYOffset > offset ? 1 : 0;
    });
  } else {
    window.addEventListener('scroll', function() {
      document.querySelector('.scroll-to-top').style.opacity = window.pageYOffset > offset ? 1 : 0;
    });
  }
  
  document.querySelector('.scroll-to-top').addEventListener('click', function(e) {
    e.preventDefault();
    scrollToTop(500);
  });

  function scrollToTop(scrollDuration) {
    var scrollStep = -window.scrollY / (scrollDuration / 15),
      scrollInterval = setInterval(function(){
        if ( window.scrollY != 0 ) {
          window.scrollBy( 0, scrollStep );
        }
        else clearInterval(scrollInterval); 
      },15);
  }
});