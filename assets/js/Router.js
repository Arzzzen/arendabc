var BackboneRouteControl = require('backbone-route-control');

module.exports = BackboneRouteControl.extend({
  routes: {
    '': 'bc#index',
    'bc/:id_or_url': 'bc#view',
    'favorites': 'bc#favorites'
  },
  notFound: function() {
    window.location = '/404.html';
  }
});