var React = require('react')
  , ReactDOM = require('react-dom')
  ;

var BCController = function(opt) {
  var app = opt.app;

  return {
    index: function () {
      var ComplexBCSearch = require('components/ComplexBCSearch/ComplexBCSearch')
        , bcCollection = app.collections.bcCollection
        , regionCollection = app.collections.regionCollection
        , metroCollection = app.collections.metroCollection
        , searchModel = app.models.search
        , roomTypeCollection = app.collections.roomTypeCollection
        , serviceCollection = app.collections.serviceCollection
        , mapCollection = app.collections.mapCollection
          ;
        document.getElementById('promo-container').style.display = 'block';

        if (bcCollection.length < 2) {
          bcCollection.fetch({data: searchModel.getData()});
        }
        regionCollection.fetchEmpty();
        metroCollection.fetchEmpty();
        roomTypeCollection.fetchEmpty();
        serviceCollection.fetchEmpty();
        mapCollection.fetchEmpty();

        ReactDOM.render(
          <ComplexBCSearch
            collection={bcCollection}
            storage={app.storage}
            searchModel={searchModel}
            regionCollection={regionCollection}
            metroCollection={metroCollection}
            roomTypeCollection={roomTypeCollection}
            serviceCollection={serviceCollection}
            mapCollection={mapCollection} />,
          document.getElementById('prime-cont')
        );
    },
    view: function(id_or_url) {
      var BusinessCenterView = require('components/BusinessCenter/BusinessCenterView')
        , bcCollection = app.collections.bcCollection
        , id = bcCollection.model.getIdFromUrl(id_or_url)
        , bc = bcCollection.get(id)
        , regionCollection = app.collections.regionCollection
        , metroCollection = app.collections.metroCollection
        , roomTypeCollection = app.collections.roomTypeCollection
        , serviceCollection = app.collections.serviceCollection
          ;
      document.getElementById('promo-container').style.display = 'none';

      !bc && app.router.notFound();
      !bc.get('full') && bc.fetch();

      ReactDOM.render(
        <BusinessCenterView
          model={bc}
          storage={app.storage}
          regionCollection={regionCollection}
          metroCollection={metroCollection}
          roomTypeCollection={roomTypeCollection}
          serviceCollection={serviceCollection} />,
        document.getElementById('prime-cont')
      );
    },
    favorites: function (argument) {
      var Favorites = require('components/Favorites')
        , BusinessCenterCollection = require('models/BusinessCenterCollection')
        , favoritesCollection = app.collections.favoritesCollection
        , storage = app.storage
        , favoriteRooms = storage.get('favoriteRooms').length ? storage.get('favoriteRooms') : [0]
        , SearchModel = require('models/SearchModel')
        , searchModel = new SearchModel({'with_room_ids': favoriteRooms})
        , regionCollection = app.collections.regionCollection
        , metroCollection = app.collections.metroCollection
        , roomTypeCollection = app.collections.roomTypeCollection
        , serviceCollection = app.collections.serviceCollection
          ;
      if (favoritesCollection.length < 2) {
        favoritesCollection.fetch({data: searchModel.getData()});
      }
      regionCollection.fetchEmpty();
      metroCollection.fetchEmpty();
      roomTypeCollection.fetchEmpty();
      serviceCollection.fetchEmpty();

      ReactDOM.render(
        <Favorites
          collection={favoritesCollection}
          storage={app.storage}
          regionCollection={regionCollection}
          metroCollection={metroCollection}
          roomTypeCollection={roomTypeCollection}
          serviceCollection={serviceCollection} />,
        document.getElementById('prime-cont')
      );
    }
  }
}

module.exports = BCController;