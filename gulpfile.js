'use strict';

var gulp = require('gulp');
var browserify = require('browserify')
var browserSync = require('browser-sync');
var source = require('vinyl-source-stream');
var $ = require('gulp-load-plugins')();
var babelify = require('babelify');
var watchify = require('watchify');
var _ = require('underscore');
var pngquant = require('imagemin-pngquant');

var paths = {
  dest: {
    js: 'public/js/',
    css: 'public/css/',
    font: 'public/fonts/',
    img: 'public/img/'
  },
  src: {
    js: 'assets/js/main.jsx',
    css: 'assets/css/main.scss',
    font: 'assets/fonts/**/*.{ttf,woff,woff2,eof,svg}',
    img: 'assets/img/**/*'
  },
  watch: {
    js: ['assets/js/**/*.*', 'components/**/*.*', 'models/**/*.*'],
    css: 'assets/css/**/*.*',
    reload: ['public/**/*.*', '**/*.jade', 'assets/js/**/*.*',]
  }
};

gulp.task('default', ['browser-sync']);

gulp.task('browser-sync', ['nodemon'], function () {
  gulp.watch(paths.watch.css, ['css']);
  gulp.watch(paths.watch.js, ['js']);

  return browserSync.init(null, {
    proxy: "http://localhost:3000",
    port: 7000,
    files: paths.watch.reload,
    open: false
  });
});

gulp.task('nodemon', function (cb) {
	
	var started = false;

	return $.nodemon({
    script: './bin/www',
    ignore: 'public/**/*.*',
    ext: 'js json jsx jade',
    env: {'NODE_PATH' : __dirname}
	}).on('start', function () {
		if (!started) {
			cb();
			started = true;
		}
	}).on('restart', jasminTasks);
});

gulp.task('js', function () {
  var opts = _.extend(watchify.args, {
    entries: [paths.src.js],
    extensions: ['.js', '.json', '.jsx'],
    debug: true,
    paths: [__dirname]
  });
	return watchify(browserify(opts))
    .transform("babelify", {presets: ["es2015", "react"] })
	  .bundle()
    .on('error', function(err) {
      console.log(err.toString());
    })
	  .pipe(source('all.min.js'))
	  .pipe(gulp.dest(paths.dest.js));
});

gulp.task('css', function () {
  return gulp.src(paths.src.css)
    .pipe($.sourcemaps.init())
    .pipe($.sass({
      outputStyle: 'nested', // libsass doesn't support expanded yet
      precision: 10,
      includePaths: ['.'],
      onError: console.error.bind(console, 'Sass error:')
    }))
    .pipe($.autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe($.sourcemaps.write())
    .pipe($.cssmin())
    .pipe($.rename('all.min.css'))
    .pipe(gulp.dest(paths.dest.css))
    .pipe(browserSync.reload({stream: true}));
});

var jasminTasks = function() {
  // return gulp.src('spec/**/*Spec.js')
  //   .pipe($.jasmine())
  //   .on('error', function(){});
}

gulp.task('test', function() {
  return jasminTasks();
});

gulp.task('font', function () {
  return gulp.src(paths.src.font)
    .pipe(gulp.dest(paths.dest.font));
});

gulp.task('img', () => {
  return gulp.src(paths.src.img)
    .pipe($.imagemin({
      progressive: true,
      svgoPlugins: [{removeViewBox: false}],
      use: [pngquant()]
    }))
    .pipe(gulp.dest(paths.dest.img));
});