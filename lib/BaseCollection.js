var Backbone = require('backbone');

module.exports = Backbone.Collection.extend({
  fetchEmpty: function(options) {
    this.length || this.fetch(options);
    return this;
  }
});