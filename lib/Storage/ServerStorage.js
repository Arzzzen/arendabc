var ServerStorage = function(req, res) {
  this.req = req;
  this.res = res;
}
ServerStorage.prototype.get = function(name) {
  var val = this.req.cookies[name];
  return (typeof val === 'string' && val.substr(0, 2) === 'j:')
    ? JSON.parse(val.substr(2))
    : val;
  return val;
}
ServerStorage.prototype.set = function(name, val) {
  return this.res.cookie(name, val);
}
ServerStorage.prototype.remove = function(name) {
  return clearCookie(name);
}

module.exports = ServerStorage;