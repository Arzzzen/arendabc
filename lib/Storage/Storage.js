var ServerStorage = require('lib/Storage/ServerStorage')
  , BrowserStorage = require('lib/Storage/BrowserStorage')
  ;
var Storage = function(req, res) {
  return !process.browser
    ? new ServerStorage(req, res)
    : new BrowserStorage();
}

module.exports = Storage;