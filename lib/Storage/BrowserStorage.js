var jsCookie = require('js-cookie')
  , Backbone = require('backbone')
  , _ = require('underscore')
    ;

var BrowserStorage = function() {};
BrowserStorage.prototype = jsCookie;
BrowserStorage.prototype.get = function(name) {
  var val = jsCookie(name);
  return (typeof val === 'string' && val.substr(0, 2) === 'j:')
    ? JSON.parse(val.substr(2))
    : val;
}
BrowserStorage.prototype.set = function(name, val) {
  val = (typeof val === 'string') ? val : 'j:'+JSON.stringify(val);
  jsCookie(name, val);
  console.log('change:'+name, val);
  this.trigger('change:'+name, val);
}
_.extend(BrowserStorage.prototype, Backbone.Events);

module.exports = BrowserStorage;