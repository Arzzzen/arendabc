var env = process.env.NODE_ENV || 'development'
  , _ = require('underscore')
    ;

module.exports = process.browser
  ? _.extend(
      require('config/common.json'),
      require('config/env/browser.json')
    )
  : _.extend(
      require('config/common'),
      require('config/config'),
      require('config/env/'+env)
    );